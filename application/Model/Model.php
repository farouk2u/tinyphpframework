<?php

namespace application\Model;

/**
 * Class Model
 */
abstract class Model {

    /**
     * Table name
     *
     * @var string
     */
    protected $table;

    /**
     * @var PDO Object
     */
    protected $db;


    public function __construct()
    {

        // Connecting to the database
        try{

            $this->db = new PDO('mysql:host=localhost;dbname=farouk', 'root');
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

        }catch(PDOException $e) {

            throw new ErrorException($e->getMessage());

        }

        if(!$this->db){

            throw new ErrorException("There is no connexion to the database");
        }


    }

    /**
     * Get record by id
     *
     * @param $id
     *
     * @return mixed
     */
    public function get($id)
    {
        $query = $this->db->query("SElECT * FROM " . $this->table . " WHERE id=$id");

        $row = $query->fetchObject();

        return $row ;

    }

    /**
     * Get all tables rows
     *
     * @return array
     * @throws ErrorException
     */
    public function getAll()
    {

        $query = $this->db->query("SELECT * FROM " . $this->table);

        $rows = $query->fetchAll();

        return $rows;
    }

    /**
     * Execute custom sql query on table ;
     *
     * @param $sqlQuery
     *
     * @return array
     */
    public function query($sqlQuery)
    {
        return array();
    }

    /**
     * Insert data as array
     *
     * @param array $data
     *
     * @return int inserted element id
     */
    public function insert($data)
    {

        // Prepare data to bind
        $cols = array();
        $bind_keys = array();
        $bind_data = array();


        foreach($data as $key => $value) {

            $cols []= $key;
            $bind_keys []= ':'.$key;
            $bind_data[':'.$key]= $value;
        }

        // Sql Query
        $sql = "INSERT INTO ". $this->table ." (". implode($cols, ', ') .")  values (". implode($bind_keys, ', ') ." )" ;
        $query = $this->db->prepare($sql);
        @$query->execute($bind_data);


        // return last inserted id
        $inserted_id = $this->db->lastInsertId();
        return  $inserted_id;

    }

    /**
     * Get the table name;
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->table;
    }

}